/* This java project dynamically handles DDL and DML operators. It allows you to 
dynamically create a table of arbitary size(variable no.of columns) and datatype.
Based on the datatype an appropriate table is created which is manipulated
via CRUD operators */




package org.apache.jsp.JSP;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("   \n");
      out.write("      \n");
      out.write("    <head>\n");
      out.write("        <title>OurDB</title>\n");
      out.write("         <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <!--setting style for the home page-->\n");
      out.write("        <style>\n");
      out.write("             /*setting background picture */\n");
      out.write("            body{\n");
      out.write("                background-image: url('bg2.jpg');\n");
      out.write("                background-size: 100% 100%;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #top{\n");
      out.write("\n");
      out.write("                background-color: rgba(10,10,77,0.6);\n");
      out.write("                height:100px;\n");
      out.write("            }\n");
      out.write("             /*customize sub button*/\n");
      out.write("\n");
      out.write("            .sub{\n");
      out.write("\n");
      out.write("                background-color: #4CAF50; /* Green */\n");
      out.write("                border: none;\n");
      out.write("                color: white;\n");
      out.write("                padding: 15px 32px;\n");
      out.write("                text-align: center;\n");
      out.write("                text-decoration: none;\n");
      out.write("                display: inline-block;\n");
      out.write("                font-size: 16px;\n");
      out.write("                margin: 4px 2px;\n");
      out.write("                cursor: pointer;\n");
      out.write("                margin-left:auto;\n");
      out.write("                margin-right:auto;\n");
      out.write("                width:100%;\n");
      out.write("            }\n");
      out.write("            #form{\n");
      out.write("                background-color:rgba(0,0,0,0.3);\n");
      out.write("                margin: auto auto;\n");
      out.write("                height:400px;\n");
      out.write("                width: 600px;\n");
      out.write("                padding: 30px 30px;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("            #heading{\n");
      out.write("                text-align: center;\n");
      out.write("                size:50px;\n");
      out.write("                color:white;\n");
      out.write("                font-family: Arial Rounded MT Bold;\n");
      out.write("                padding: 30px;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("              /*customize hover of sub button*/\n");
      out.write("            .sub:hover {\n");
      out.write("                background: #434343;\n");
      out.write("                letter-spacing: 5px;\n");
      out.write("                -webkit-box-shadow: 0px 5px 40px -10px rgba(20,36,50,0.57);\n");
      out.write("                -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);\n");
      out.write("                box-shadow: 5px 40px -10px rgba(0,0,0,0.57);\n");
      out.write("                transition: all 0.4s ease 0s;\n");
      out.write("            }\n");
      out.write("            .btn{\n");
      out.write("                margin-top: 50px;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         <div id=\"form\">\n");
      out.write("        <div id=\"top\"><h1 id=\"heading\">Welcome To Database Creator</h1></div>\n");
      out.write("        <div class=\"btn\">\n");
      out.write("            <!--mapping this submit type input to createIt.jsp page-->\n");
      out.write("        <input class=\"sub\" type=\"submit\" value=\"Create\" name=\"create\" onclick=\"location.href = 'createIt.jsp';\" >\n");
      out.write("        <!--mapping this submit type input to Operations page-->\n");
      out.write("        <input class=\"sub\" type=\"submit\" value=\"Operations\" name=\"Operations\" onclick=\"location.href = 'operations.jsp';\">\n");
      out.write("    \n");
      out.write("        </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
